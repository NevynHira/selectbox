//# sourceURL=selectbox.js
/*globals $*/
"use strict";

function Selectbox( select_box ){
    this.selectbox = select_box;
    this.selectbox.append("<table></table>");
    this.multiSelect = this.selectbox.hasClass("multiselect");
    this.headings = [];
    this.fields = [];

    // private methods
    this.isJSON = function (str){
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    };

    this.xor = function( a, b ){
        return(( a && !b ) || (!a && b));
    };
}

Selectbox.prototype.getRows = function( id ){
    if( id !== undefined ){
        var rows = this.selectbox.find("table tbody tr");
        if( id.constructor == Array ){
            return rows.filter(function( a ){
                return id.indexOf( rows.eq(a).data("id") ) != -1;
            });
        }
        else{
            return rows.filter( function(a){
                return rows.eq(a).data("id") == id;
            });
        }
    }
    else{
        return this.selectbox.find("table tbody tr");
    }
};

Selectbox.prototype.sortAsc = function( column ){
    if( column === undefined ){
        column = 0;
    }
    var sel = this.getRows();
    sel.sort( function( a, b ){
        var aval = $(a).find("td").eq(column).html().toUpperCase();
        var bval = $(b).find("td").eq(column).html().toUpperCase();
        if( aval < bval ){
            $(a).after(b);
            return -1;
        }
        else if( aval > bval ){
            $(b).after(a);
            return 1;
        }
        else{
            return 0;
        }
    });
};

Selectbox.prototype.sortDes = function( column ){
    if( column === undefined ){
        column = 0;
    }
    var sel = this.getRows();
    sel.sort( function( a, b ){
        var aval = $(a).find("td").eq(column).html().toUpperCase();
        var bval = $(b).find("td").eq(column).html().toUpperCase();
        if( aval == bval ){
            return(0);
        }
        else if( aval < bval ){
            $(b).after(a);
            return(1);
        }
        else{
            $(a).after(b);
            return(-1);
        }
    });
};

Selectbox.prototype.clear = function(){
    this.getRows().off();
    this.selectbox.find( "table" ).remove();
};

Selectbox.prototype.selectEvent = function( event ){
    var row = $(event.target).closest("tr");
    if ( this.multiSelect && event.ctrlKey ){
        row.toggleClass( "selected_item" );
    }
    else{
        this.selectbox.find("tr").removeClass("selected_item");
        row.addClass("selected_item");
    }
};

Selectbox.prototype.getSelectedID = function(){
    var selected = this.getSelectedRow();
    if( this.multiSelect ){
        var return_value = selected.map(function( a ){
            return selected.eq(a).data("id");
        });
        return $.makeArray(return_value);
    }
    else{
        if( selected ){
            return(selected.data("id"));
        }
        else{
            return false;
        }
    }
};

Selectbox.prototype.getData = function( datafield, row ){
    // This needs a rewrite... It's currently horribly confused.
    if ( this.multiSelect ){
        return row.map(function(){
            return $(this).data( datafield );
        }).get();
    }
    else{
        return row.data( datafield );
    }
};

Selectbox.prototype.getSelectedRow = function(){
    return this.getRows().filter( ".selected_item" );
};

Selectbox.prototype.getCell = function( id, column ){
    if( column === undefined ){
        column = 0;
    }
    var row = this.getRows(id);
    return row.find("td:eq(" + column +")").html();
};

Selectbox.prototype.repopulate = function( json_data ){
    var selected_rows = this.getSelectedID();
    var visible_columns = [];
    $.each( this.selectbox.find('thead td'), $.proxy( function( index, cell){
        if( $(cell).css('display')!="none" ){
            visible_columns.push(index);
        }
    }, this ));
    this.populate( json_data );
    $.each( this.selectbox.find('thead td'), $.proxy( function( index, cell){
        if( visible_columns.includes(index) ){
            $(this.selectbox).find('td:nth-child('+(index+1)+')').show();
        }
        else{
            $(this.selectbox).find('td:nth-child('+(index+1)+')').hide();
        }
    }, this ));
    if( selected_rows !== undefined ){
        var select_rows = this.getRows(selected_rows);
        if( select_rows !== undefined ){
            select_rows.addClass( "selected_item" );
        }
    }
};

Selectbox.prototype.getHiddenColumns = function(){
    var columns = this.selectbox.find("thead").find('td');
    var returnvalue = [];
    $.each(columns, function(index, value){
        if( !$(value).is(':visible')){
            returnvalue.push(index+1);
        }
    });
    return(returnvalue);
};

Selectbox.prototype.getVisibleColumns = function(){
    var columns = this.selectbox.find("thead").find('td');
    var returnvalue = [];
    $.each(columns, function(index, value){
        if( $(value).is(':visible')){
            returnvalue.push(index+1);
        }
    });
    return(returnvalue);
};

Selectbox.prototype.populateHeadings = function( headings ){
    var table = this.selectbox.find( "table" );
    if(table.find("thead").length){
        table.find("thead").remove();
    }
    table.append(document.createElement("thead"));
    table.find( "thead" ).append(document.createElement("tr"));
    this.headings = [];
    var column_index;
    column_index = 0;
    for( var heading in headings ){
        var head = document.createElement("td");
        $(head).addClass('column'+column_index);
        column_index=column_index+1;
        $(head).html(headings[heading]);
        this.headings[this.headings.length] = headings[heading];
        table.find('thead tr').append(head);
    }
	var last_sorted_column;
    last_sorted_column=0;
    var sort_direction;
    sort_direction='asc';
    this.selectbox.find('thead td').off();
    this.selectbox.find('thead td').on('click', $.proxy(function(e){
        if( last_sorted_column==$(e.target).index()){
            if( sort_direction=='asc'){
                this.sortDes($(e.target).index());
                sort_direction="des";
            }
            else{
                this.sortAsc($(e.target).index());
                sort_direction="asc";
            }
        }
        else{
            this.sortAsc($(e.target).index());
            sort_direction='asc';
        }
        last_sorted_column=$(e.target).index();
    }, this));
};

Selectbox.prototype.populate = function( json_data ){
    //populate the select box. Json should be formatted as:
    //  {
    //      "headings":{
    //          'column_heading 1' : 'data_field name 1',
    //          'column_heading 2' : 'data_field name 2'
    //      },
    //      "fields":{
    //          "type":"Boolean",
    //          See Fields object below for more details
    //      },
    //      uid:{
    //          "col1":"value",
    //          "col2":"value",
    //      },
    //      uid:{
    //          "col1":"value",
    //          "col2":"value",
    //      }
    //  }
    //  uid is an identifier for that row/piece of information.
    //  column keys are for the most part ignored except:
    //    * if they begin with 'data-', they will be added to the row as a 
    //      'data-' attribute and can be accessed via jquery using the data
    //      method
    //    * 'class' is applied to the row as a class. This allows, for example,
    //       styles to be applied to a row. Apply default styes to tr, and 
    //       styles to .classname
    //    * Headings MUST have a datafield name associated to it. This makes
    //      the order of columns a presentation/layout issue rather than
    //      the responsibility of the order in which data comes in in.
    var data;
    // If the developer needs to change something before passing the data
    // to the selectbox method, then they'd likely have to convert the data
    // from JSON, make the changes... and then pass the data back. Which is
    // silly because the first thing that's done is to convert the data from
    // JSON.
    // We can afford to be a little flexible here and allow the developer to
    // throw the data at us and hope like hell it's not rubbish.
    if( this.isJSON( json_data ) ){
        data = JSON.parse( json_data );
    }
    else{
        data = json_data;
    }
    var table = this.selectbox.find( "table" );
    //reset tbody
    if(table.find("tbody").length){
        table.find("tbody").remove();
    }
    table.append(document.createElement("tbody"));
    if( "headings" in data ){
        this.populateHeadings( data.headings );
        delete( data.headings );
    }
    if( "fields" in data ){
        this.populatefields( data.fields );
        delete data.fields;
    }
    for( var index in data ){
        var row = $(document.createElement( "tr" ));
        var row_data = data[index];
        var cell_keys = Object.keys( data[index] );
        // var data_fields = Array();

        for( var key_index in cell_keys ){ 
            if( cell_keys[key_index].startsWith( 'data-' )){
                // data_fields.push( cell_keys.splice( key_index,1 )[0] );
                row.data( cell_keys[key_index].replace(/^(data-)/,""),
                        row_data[cell_keys[key_index]]);
            }
        }

        if( cell_keys.indexOf('class') > -1 ){
            row.addClass( row_data['class'] );
            cell_keys.splice( cell_keys.indexOf('class'),1 );
        }
        row.data( "id", index );

		var column_index=0;
        for( var column in this.headings ){
            var cell = $(document.createElement("td"));
            $(cell).addClass('column'+column);
            var field = this.searchHeadings( this.headings[column] );
            if(field.length>0){
                if( field[0].type == "String" ){
                    cell.html(row_data[field[0].datafield]);
                }
                else if (field[0].type == "Boolean"){
                    // if condition...
                    // Equal to / Not equal to string value
                    // Equal, bigger than, smaller than a numerical value?
                    var value;
                    if(field[0].if_condition[0] == '='){
                        value = (row_data[field[0].datafield] == field[0].if_condition.substring(1));
                    }
                    else if (field[0].charAt[0] == '>'){
                        value = (field[0].if_condition.substring(1)>row_data[field[0].datafield]);
                    }
                    else if(field[0].charAt[0] == '<' ){
                        value = (field[0].if_condition.substring(1) < row_data[field[0].datafield]);
                    }
                    else{
                        console.log("Error in fields definition. Condition doesn't start with either =, < or >");
                    }
                    if( ! this.xor( value, field[0].if_condition_tf )){
                        cell.html(field[0].true_display);
                    }
                    else{
                        cell.html(field[0].false_display);
                    }
                }
            }
            row.append( cell );
        }
        $(this.selectbox).find("tbody").append( row );
    }
    this.getRows().off();
    this.getRows().on( "mousedown", $.proxy( this.selectEvent, this ));
};

Selectbox.prototype.populateFields = function( fields ){
    for( var field in fields ){
        this.fields.push(new Field(fields[field].type));
        this.fields[this.fields.length-1].initialize(fields[field]);
    }
};

function Field( type ){
    if (type===""){
        type="String";
    }
    // this.heading;
    // this.data_field;
    this.type = type;
}

Field.prototype.initialize = function( field ){
    this.type = field.type;
    this.heading = field.heading;
    this.datafield = field.datafield;
    if( this.type == "Boolean" ){
        // field.true gives 

        if( "true" in field ){
            this.true_display = field.true;
        }
        else{
            this.true_display = "True";
        }
        if( "false" in field ){
            this.false_display = field.false;
        }
        else{
            this.false_display = "False";
        }
        if( "condition" in field ){
            this.if_condition = field.condition;
            if( "condition_tf" in field ){
                this.if_condition_tf = field.condition_tf;
            }
            else{
                this.if_condition_tf = "true";
            }
        }
        else{
            this.if_condition = '=1';
            this.if_condition_tf = true;
        }
    }
};

Selectbox.prototype.searchHeadings = function( heading ){
    return this.fields.filter(function(el){
        return el.heading == heading;
    });
};